/* SPDX-License-Identifier: Apache-2.0 */

/*
 * File:             include/molfar/core.h
 * Created:          10-02-2019
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2019 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MOLFAR_CORE_H
#define MOLFAR_CORE_H 1


/**
 * @defgroup molfar_core core
 * @brief Base functions and types.
 * @{
 */

/**
 * @defgroup core_collections Collections
 * @brief Arrays, lists, trees, iterators, etc.
 *
 * This is the reference documentation for molfar collections. For more
 * task-oriented information, see the @ref core_collections_guide.
 */

/**
 * @}
 */

#include <molfar/core/array.h>
#include <molfar/core/lib.h>
#include <molfar/core/lib.h>
#include <molfar/core/memory.h>
#include <molfar/core/panic.h>
#include <molfar/core/primitive_types.h>
#include <molfar/core/status.h>
#include <molfar/core/subsystems.h>
#include <molfar/core/type.h>

#endif /* MOLFAR_CORE_H */

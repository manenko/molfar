/* SPDX-License-Identifier: Apache-2.0 */

/*
 * File:             include/molfar/primitive_types.h
 * Created:          10-02-2019
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2019 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MOLFAR_CORE_PRIMITIVE_TYPES_H
#define MOLFAR_CORE_PRIMITIVE_TYPES_H 1

#include <molfar/core/lib.h>

#include <float.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

MOLFAR_CORE_EXTERN_C_BEGIN

/**
 * @file
 *
 * @brief Integer and floating-point numbers of fixed size, pointer-sized
 *        numbers, etc.
 */

/* Signed integer types.
 * -------------------------------------------------------------------------- */

/**
 * @brief The 8-bit signed integer type.
 *
 * - Minimum value: #I8_MIN
 * - Maximum value: #I8_MAX
 *
 * @sa #i64, #u64, #i32, #u32, #i16, #u16, #u8, and #usize
 */
typedef int8_t i8;

/**
 * @brief The 16-bit signed integer type.
 *
 * - Minimum value: #I16_MIN
 * - Maximum value: #I16_MAX
 *
 * @sa #i64, #u64, #i32, #u32, #u16, #i8, #u8, and #usize
 */
typedef int16_t i16;

/**
 * @brief The 32-bit signed integer type.
 *
 * - Minimum value: #I32_MIN
 * - Maximum value: #I32_MAX
 *
 * @sa #i64, #u64, #u32, #i16, #u16, #i8, #u8, and #usize
 */
typedef int32_t i32;

/**
 * @brief The 64-bit signed integer type.
 *
 * - Minimum value: #I64_MIN
 * - Maximum value: #I64_MAX
 *
 * @sa #u64, #i32, #u32, #i16, #u16, #i8, #u8, and #usize
 */
typedef int64_t i64;

/* Unsigned integer types.
 * -------------------------------------------------------------------------- */

/**
 * @brief The 8-bit unsigned integer type.
 *
 * - Minimum value: #U8_MIN
 * - Maximum value: #U8_MAX
 *
 * @sa #i64, #u64, #i32, #u32, #i16, #u16, #i8, and #usize
 */
typedef uint8_t u8;

/**
 * @brief The 16-bit unsigned integer type.
 *
 * - Minimum value: #U16_MIN
 * - Maximum value: #U16_MAX
 *
 * @sa #i64, #u64, #i32, #u32, #i16, #i8, #u8, and #usize
 */
typedef uint16_t u16;

/**
 * @brief The 32-bit unsigned integer type.
 *
 * - Minimum value: #U32_MIN
 * - Maximum value: #U32_MAX
 *
 * @sa #i64, #u64, #i32, #i16, #u16, #i8, #u8, and #usize
 */
typedef uint32_t u32;

/**
 * @brief The 64-bit unsigned integer type.
 *
 * - Minimum value: #U64_MIN
 * - Maximum value: #U64_MAX
 *
 * @sa #i64, #i32, #u32, #i16, #u16, #i8, #u8, and #usize
 */
typedef uint64_t u64;

/* Pointer-sized integer types.
 * -------------------------------------------------------------------------- */

/**
 * @brief The pointer-sized unsigned integer type. It has the same
 *        size as a pointer on a target machine.
 *
 * - Minimum value: #USIZE_MIN
 * - Maximum value: #USIZE_MAX
 *
 * @sa #i64, #u64, #i32, #u32, #i16, #u16, #i8, and #u8.
 */
typedef size_t usize;

/* Floating-point types.
 * -------------------------------------------------------------------------- */

/**
 * @brief The 32-bit floating-point type.
 *
 * - Minimum normalized positive value: #F32_MIN_POSITIVE
 * - Maximum finite value: #F32_MAX
 * - Epsilon: #F32_EPSILON
 *
 * @sa #f64
 */
typedef float f32;

/**
 * @brief The 64-bit floating-point type.
 *
 * - Minimum normalized positive value: #F64_MIN_POSITIVE
 * - Maximum finite value: #F64_MAX
 * - Epsilon: #F64_EPSILON
 *
 * @sa #f32
 */
typedef double f64;

/* Constants for signed integer types.
 * -------------------------------------------------------------------------- */

/**
 * @brief The smallest value that can be represented by the #i8 integer type.
 *
 * @sa #i8, #I8_MAX
 */
#define I8_MIN INT8_MIN

/**
 * @brief The largest value that can be represented by the #i8 integer type.
 *
 * @sa #i8, #I8_MIN
 */
#define I8_MAX INT8_MAX

/**
 * @brief The smallest value that can be represented by the #i16 integer type.
 *
 * @sa #i16, #I16_MAX
 */
#define I16_MIN INT16_MIN

/**
 * @brief The largest value that can be represented by the #i16 integer type.
 *
 * @sa #i16, #I16_MIN
 */
#define I16_MAX INT16_MAX

/**
 * @brief The smallest value that can be represented by the #i32 integer type.
 *
 * @sa #i32, #I32_MAX
 */
#define I32_MIN INT32_MIN

/**
 * @brief The largest value that can be represented by the #i32 integer type.
 *
 * @sa #i32, #I32_MIN
 */
#define I32_MAX INT32_MAX

/**
 * @brief The smallest value that can be represented by the #i64 integer type.
 *
 * @sa #i64, #I64_MAX
 */
#define I64_MIN INT8_MIN

/**
 * @brief The largest value that can be represented by the #i64 integer type.
 *
 * @sa #i64, #I64_MIN
 */
#define I64_MAX INT64_MAX

/* Constants for unsigned integer types.
 * -------------------------------------------------------------------------- */

/**
 * @brief The smallest value that can be represented by the #u8 integer type.
 *
 * @sa #u8, #U8_MAX
 */
#define U8_MIN UINT8_MIN

/**
 * @brief The largest value that can be represented by the #u8 integer type.
 *
 * @sa #u8, #U8_MIN
 */
#define U8_MAX UINT8_MAX

/**
 * @brief The smallest value that can be represented by the #u16 integer type.
 *
 * @sa #u16, #U16_MAX
 */
#define U16_MIN UINT16_MIN

/**
 * @brief The largest value that can be represented by the #u16 integer type.
 *
 * @sa #u16, #U16_MIN
 */
#define U16_MAX UINT16_MAX

/**
 * @brief The smallest value that can be represented by the #u32 integer type.
 *
 * @sa #u32, #U32_MAX
 */
#define U32_MIN UINT32_MIN

/**
 * @brief The largest value that can be represented by the #u32 integer type.
 *
 * @sa #u32, #U32_MIN
 */
#define U32_MAX UINT32_MAX

/**
 * @brief The smallest value that can be represented by the #u64 integer type.
 *
 * @sa #u64, #U64_MAX
 */
#define U64_MIN UINT8_MIN

/**
 * @brief The largest value that can be represented by the #u64 integer type.
 *
 * @sa #u64, #U64_MIN
 */
#define U64_MAX UINT64_MAX

/* Constants for floating-point types.
 * -------------------------------------------------------------------------- */

/**
 * @brief The smallest normalized positive value that can be
 *        represented by the #f32 floating-point type.
 *
 * @sa #f32, #F32_MAX, and #F32_EPSILON.
 */
#define F32_MIN_POSITIVE FLT_MIN

/**
 * @brief The largest finite value that can be represented by the #f32
 *        floating-point type.
 *
 * @sa #f32, #F32_MIN_POSITIVE, and #F32_EPSILON.
 */
#define F32_MAX FLT_MAX

/**
 * @brief The smallest normalized positive value that can be
 *        represented by the #f64 floating-point type.
 *
 * @sa #f64, #F64_MAX, and #F64_EPSILON.
 */
#define F64_MIN_POSITIVE DBL_MIN

/**
 * @brief The largest finite value that can be represented by the #f64
 *        floating-point type.
 *
 * @sa #f64, #F64_MIN_POSITIVE, and #F64_EPSILON.
 */
#define F64_MAX DBL_MAX

/**
 * @brief Difference between 1.0 and the next representable value for #f32.
 *
 * @sa #f32, #F32_MIN_POSITIVE, and #F32_MAX.
 */
#define F32_EPSILON FLT_EPSILON

/**
 * @brief Difference between 1.0 and the next representable value for #f64.
 *
 * @sa #f64, #F64_MIN_POSITIVE, and #F64_MAX.
 */
#define F64_EPSILON DBL_EPSILON

/* Constants for pointer-sized integer types.
 * -------------------------------------------------------------------------- */

/**
 * @brief The smallest value that can be represented by the #usize integer type.
 *
 * @sa #usize, #USIZE_MAX
 */
#define USIZE_MIN 0

/**
 * @brief The largest value that can be represented by the #usize integer type.
 *
 * @sa #usize, #USIZE_MIN
 */
#define USIZE_MAX SIZE_MAX

MOLFAR_CORE_EXTERN_C_END

#endif /* MOLFAR_CORE_PRIMITIVE_TYPES_H */

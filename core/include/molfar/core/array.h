/* SPDX-License-Identifier: Apache-2.0 */

/*
 * File:             include/molfar/core/array.h
 * Created:          13-02-2019
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2019 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MOLFAR_CORE_ARRAY_H
#define MOLFAR_CORE_ARRAY_H 1

#include <molfar/core/lib.h>
#include <molfar/core/memory.h>
#include <molfar/core/primitive_types.h>
#include <molfar/core/status.h>
#include <molfar/core/type.h>

MOLFAR_CORE_EXTERN_C_BEGIN

/**
 * @file
 *
 * @brief An ordered, random-access collection.
 */

/**
 * @addtogroup core_collections
 *
 * @{
 */

/**
 * @defgroup core_array coreArray
 * @brief An ordered, random-access collection.
 *
 * This is the reference documentation for @a coreArray. For more
 * task-oriented information, see the @ref core_array_guide.
 *
 * @{
 */

/**
 * @brief Opaque type for an ordered, random-access collection.
 */
typedef struct coreArray coreArray;


MOLFAR_CORE_EXPORT
coreArray*
coreArrayCreate(coreType const* elementType, coreMemoryAllocator* allocator);

MOLFAR_CORE_EXPORT
coreArray*
coreArrayCreateWithCapacity(coreType const*      elementType,
                            usize                capacity,
                            coreMemoryAllocator* allocator);

MOLFAR_CORE_EXPORT
void
coreArrayDestroy(coreArray* array);

MOLFAR_CORE_EXPORT
coreStatus
coreArrayAppend(coreArray* array, void* element);

MOLFAR_CORE_EXPORT
coreStatus
coreArrayInsertAt(coreArray* array, void* element, usize index);

MOLFAR_CORE_EXPORT
void*
coreArrayGetAt(coreArray* array, usize index);

MOLFAR_CORE_EXPORT
coreStatus
coreArraySetAt(coreArray* array, void* element, usize index);

MOLFAR_CORE_EXPORT
usize
coreArrayGetLength(coreArray const* array);

MOLFAR_CORE_EXPORT
usize
coreArrayGetCapacity(coreArray const* array);

MOLFAR_CORE_EXPORT
usize
coreArrayGetElementSize(coreArray const* array);

MOLFAR_CORE_EXPORT
bool
coreArrayIsEmpty(coreArray const* array);

MOLFAR_CORE_EXPORT
coreStatus
coreArrayReserve(coreArray* array, usize capacity);

MOLFAR_CORE_EXPORT
coreStatus
coreArrayResize(coreArray* array, usize length);

MOLFAR_CORE_EXPORT
void
coreArrayRemoveAt(coreArray* array, usize index);

/**
 * @}
 */

/**
 * @}
 */

MOLFAR_CORE_EXTERN_C_END

#endif /* MOLFAR_CORE_ARRAY_H */

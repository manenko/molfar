/* SPDX-License-Identifier: Apache-2.0 */

/*
 * File:             include/molfar/core/status.h
 * Created:          16-02-2019
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2019 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MOLFAR_CORE_STATUS_H
#define MOLFAR_CORE_STATUS_H 1

#include <molfar/core/lib.h>
#include <molfar/core/primitive_types.h>

MOLFAR_CORE_EXTERN_C_BEGIN

/**
 * @file
 *
 * @brief Operations status codes.
 */

/**
 * @brief A completion status of an operation.
 *
 * The @c coreStatus supports the following status types:
 *
 * @li @a Success. The operation completed successfully.
 * @li @a Failure. The operation failed.
 *
 * ## coreStatus Format
 *
 * The @c coreStatus is a 32-bits signed integer:
 *
 * @code{.unparsed}
 * Bit 31: Failure/Success
 *    /
 *   /          Bits 15-0: Status code
 *  /                       \
 * +                         \
 * |                 +-------+-------+
 * |                 |               |
 * v                 v               v
 * 00000000.00000000.00000000.00000000
 *  ^              ^
 *  |              |
 *  +------+-------+
 *         |
 *         +
 *         /
 *        /
 *    Bits 30-16: Subsystem.
 * @endcode
 *
 * ### Failure/Success
 *
 * Bit @c 31 is @c 0 if operation was successful, and 1 otherwise.  In other
 * words, a successfull result is zero or a positive number, and failure is
 * negative.
 *
 * Use #coreStatusIsSuccess and #coreStatusIsFailure to check this bit.
 *
 * Use #coreStatusCreateSuccess and #coreStatusCreateFailure to create
 * successful/failed statuses.
 *
 * ### Subsystem
 *
 * Bits @c 30 - @c 16 define a subsystem code which occupies 15 bits.  Bit @c 30
 * is @c 0 for @a molfar libraries.  All other libraries should set it to @c 1.
 *
 * Use #coreStatusGetSubsystem to get a subsystem code from the given @a status.
 *
 * Use #coreStatusIsMolfarSubsystem to check if the given @a status belongs to
 * @a molfar libraries.
 *
 * ### Status Code
 *
 * Bits @c 15 - @c 0 define a status code which is unique number that represents
 * the current status. Use #coreStatusGetCode to get these bits.
 */
typedef i32 coreStatus;

typedef u16 coreStatusSubsystem;

typedef u16 coreStatusCode;

/**
 * @brief Creates a status from the given @a type, @a subsystem, and @a code.
 *
 * @param[in]  type       The status type. 0 is success, 1 is failure.
 * @param[in]  subsystem  The subsystem that is responsible for the status.
 * @param[in]  code       The status code.
 *
 * @returns #coreStatus constructed from the given @a type, @a subsystem, and
 *          @a code.
 */
#define coreStatusCreate(type, subsystem, code) ((coreStatus)(code))

#define coreStatusCreateSuccess(subsystem, code)                               \
    coreStatusCreate(0, subsystem, code)

#define coreStatusCreateFailure(subsystem, code)                               \
    coreStatusCreate(1, subsystem, code)

#define coreMolfarSubsystemCreate(code) (((coreStatusSubsystem)(code)) & 0xBFFF)

inline coreStatusSubsystem
coreStatusGetSubsystem(coreStatus status)
{
    return (status >> 0xF) & 0x7FFF;
}

inline bool
coreStatusIsMolfarSubsystem(coreStatus status)
{
    return (status & 0x40000000) == 0;
}

inline coreStatusCode
coreStatusGetCode(coreStatus status)
{
    return status & 0xFFFF;
}

inline bool
coreStatusIsSuccess(coreStatus status)
{
    return status >= 0;
}

inline bool
coreStatusIsFailure(coreStatus status)
{
    return status < 0;
}

MOLFAR_CORE_EXTERN_C_END

#endif /* MOLFAR_CORE_STATUS_H */

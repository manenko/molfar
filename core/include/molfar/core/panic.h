/* SPDX-License-Identifier: Apache-2.0 */

/*
 * File:             include/molfar/core/panic.h
 * Created:          10-02-2019
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2019 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MOLFAR_CORE_PANIC_H
#define MOLFAR_CORE_PANIC_H 1

#include <molfar/core/lib.h>

#include <stdio.h>
#include <stdlib.h>

MOLFAR_CORE_EXTERN_C_BEGIN

/**
 * @file
 *
 * @brief Terminate a program immediately and provide feedback to the
 *        caller.
 *
 * Use the functions and macros from this file to terminate a program when it
 * reaches an unrecoverable problem.
 */

/**
 * @addtogroup molfar_core
 *
 * @{
 */

/**
 * @defgroup core_panics Panics
 * @brief Terminate a program if it reaches an unrecoverable problem.
 *
 * @details
 *
 * There are problems a program cannot recover from.  The module provides a few
 * functions to terminate it and provide as much contextual information as
 * possible:
 *
 * - #corePanic
 * - #corePanicIfNull
 * - #corePanicNotImplemented
 *
 * The #corePanic function is the most generic one among other panics and has
 * extensive documentation with examples. Go on and read it.
 *
 * @{
 */


#ifndef DOXYGEN_SHOULD_SKIP_THIS
#define _core_panic_va_args_head(...) _core_panic_va_args_head_0(__VA_ARGS__, 0)
#define _core_panic_va_args_head_0(_0, ...) _0

#define _core_panic_va_args_tail(...) _core_panic_va_args_tail_0(__VA_ARGS__, 0)
#define _core_panic_va_args_tail_0(_0, ...) __VA_ARGS__

#define _core_panic(formatString, ...)                                         \
    do {                                                                       \
        fprintf(stderr,                                                        \
                "'%s' panicked at '" formatString "', %s:%d\n",                \
                __func__,                                                      \
                __VA_ARGS__,                                                   \
                __FILE__,                                                      \
                __LINE__);                                                     \
        fflush(stderr);                                                        \
        abort();                                                               \
    } while (0)
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

/**
 * @brief Prints the given formatted data (if any) to the standard
 *        error stream and immediately terminates the program.
 *
 * Prints standard message if there is no formatted data provided.
 * The formatted data is the same data you use for [printf] function,
 * i.e. format string followed by additional arguments.
 *
 * ## Examples
 *
 * You can call `corePanic` without arguments:
 *
 * @code{.c}
 * #include <molfar/core/panic.h>
 *
 * int
 * main(int argc, char **argv)
 * {
 * 	corePanic();
 * }
 * @endcode
 *
 * Compile and run. The program will terminate with something similar to the
 * following output:
 *
 * @code{.unparsed}
 * 'main' panicked at '',
 * /home/vault-boy/Documents/Projects/mlfr/source/main.c:29 [1] 65327
 * abort      ./mlfr
 * @endcode
 *
 * The first line is the standard error message provided by `corePanic`. It
 * includes a function name - `main`, path to the source file `.../main.c` and
 * the line `29`.
 *
 * You can call the function with a single string argument:
 *
 * @code{.c}
 * corePanic("unexpected error");
 * @endcode
 *
 * The @c panic will include this text in its message:
 *
 * @code{.unparsed}
 * 'main' panicked at 'unexpected error',
 * /home/vault-boy/Documents/Projects/mlfr/source/main.c:29
 * @endcode
 *
 * You can invoke the function with more than one argument:
 *
 * @code{.c}
 * corePanic("the given index is %d, "
 *           "but the length of the array is %d", 42, 12);
 * @endcode
 *
 * Which gives us:
 *
 * @code{.unparsed}
 * 'main' panicked at 'the given index is 42 but the length of the array is 12',
 * /home/vault-boy/Documents/Projects/mlfr/source/main.c:29
 * @endcode
 *
 * ## Customising a source file path
 *
 * The function uses `__FILE__` to get a path to the source file. The contents
 * of that variable depends on a compiler and its flags, i.e. there is no
 * standard way to force relative/absolute paths. There is a trick, though: put
 * the `#line` directive to a source file right after the `#include` block and
 * provide path to the file explicitly:
 *
 * @code{.c}
 * #include <molfar/core/panic.h>
 * #include <another/usefull/header.h>
 *
 * #line __LINE__ "my/path/to/file.c"
 *
 * void panic()
 * {
 *     corePanic();
 * }
 * @endcode
 *
 * The `corePanic` will print "my/path/to/file.c" as a source file name.
 *
 * @todo Provide support for backtraces.
 * @todo Make a stream the #corePanic writes to configurable.
 *
 * @see #corePanicIfNull and #corePanicNotImplemented.
 *
 * [printf]: https://www.gnu.org/software/libc/manual/html_node/Formatted-Output.html
 */
#define corePanic(...)                                                         \
    _core_panic(_core_panic_va_args_head(__VA_ARGS__) "%.0d",                  \
                _core_panic_va_args_tail(__VA_ARGS__))

/**
 * @brief Panics with "not implemented" messsage.
 *
 * @see #corePanic and #corePanicIfNull.
 */
#define corePanicNotImplemented() corePanic("not implemented")

/**
 * @brief Panics with "NULL pointer" message if the given @a ptr is @c NULL.
 *
 * @param[in] ptr A pointer to check for @c NULL.
 *
 * @see #corePanic and #corePanicNotImplemented.
 */
#define corePanicIfNull(ptr)                                                   \
    if (NULL == (ptr))                                                         \
    corePanic("NULL pointer")

/**
 * @}
 */

/**
 * @}
 */

MOLFAR_CORE_EXTERN_C_END

#endif /* MOLFAR_CORE_PANIC_H */

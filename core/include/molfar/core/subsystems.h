/* SPDX-License-Identifier: Apache-2.0 */

/*
 * File:             include/molfar/core/subsystems.h
 * Created:          16-02-2019
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2019 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MOLFAR_CORE_SUBSYSTEMS_H
#define MOLFAR_CORE_SUBSYSTEMS_H 1

#include <molfar/core/lib.h>
#include <molfar/core/status.h>

MOLFAR_CORE_EXTERN_C_BEGIN

/**
 * @file
 *
 * @brief Core subsystems metadata.
 */

typedef enum coreSubsystems
{
    coreSubsystemCore   = coreMolfarSubsystemCreate(0x0),
    coreSubsystemMemory = coreMolfarSubsystemCreate(0x1)
} coreSubsystems;

#define CORE_SUCCESS coreStatusCreateSuccess(coreSubsystemCore, 0x0)

#define CORE_FAILURE coreStatusCreateFailure(coreSubsystemCore, 0x0)

MOLFAR_CORE_EXTERN_C_END

#endif /* MOLFAR_CORE_SUBSYSTEMS_H */

/* SPDX-License-Identifier: Apache-2.0 */

/*
 * File:             include/molfar/core/memory.h
 * Created:          10-02-2019
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2019 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MOLFAR_CORE_MEMORY_H
#define MOLFAR_CORE_MEMORY_H 1

#include <molfar/core/lib.h>
#include <molfar/core/primitive_types.h>
#include <molfar/core/status.h>
#include <molfar/core/subsystems.h>

MOLFAR_CORE_EXTERN_C_BEGIN

/**
 * @file
 *
 * @brief Dynamic memory management.
 */

/**
 * @addtogroup molfar_core
 *
 * @{
 */

/**
 * @defgroup core_memory Memory management
 * @brief Dynamic memory management functions and types.
 *
 * @details
 *
 * ### Memory management functions
 *
 * The module provides you with a few polymorphic memory management
 * functions:
 *
 * - #coreAllocateMemory
 * - #coreReallocateMemory
 * - #coreFreeMemory
 *
 * Each of them accepts a pointer to the #coreMemoryAllocator and delegates the
 * actual job of memory management to it:
 *
 * @code{.c}
 *
 * #include <molfar/core.h>
 *
 * coreMemoryAllocator allocator = yourMemoryAllocatorCreate();
 * u8* byteArray = coreAllocateMemory(&allocator, 100, sizeof(u8));
 * if (coreIsNotNull(byteArray)) {
 *     doSomethingWithArray(byteArray);
 *     coreFreeMemory(allocator, byteArray);
 * }
 * @endcode
 *
 * The allocator could be a `NULL` pointer which means the default system
 * allocator will be used:
 *
 * @code{.c}
 *
 * #include <molfar/core.h>
 *
 * u8* byteArray = coreAllocateMemory(NULL, 100, sizeof(u8));
 * if (coreIsNotNull(byteArray)) {
 *     doSomethingWithArray(byteArray);
 *     coreFreeMemory(NULL, byteArray);
 * }
 * @endcode
 *
 * ### Check if a pointer is `NULL`
 *
 * There are a few functions to check if the given pointer is (not) `NULL`:
 *
 * - #coreIsNull
 * - #coreIsNotNull
 *
 * One may prefer to use them instead of the following code:
 *
 * @code{.c}
 * void* ptr = NULL;
 * if (ptr) {
 *     doSomething(ptr);
 * }
 * @endcode
 *
 * The functions could make `NULL`-checks a bit more visibile and explicit.
 *
 * @code{.c}
 * void* ptr = NULL;
 * if (coreIsNotNull(ptr)) {
 *     doSomething(ptr);
 * }
 * @endcode
 *
 * Anyway, it's a matter of personal taste.
 *
 * ### Memory allocator
 *
 * Memory allocator provides a flexibility for a caller to modify the
 * fundamental memory allocation functions used by a callee.  For example, every
 * @ref core_collections "collection" requires an allocator to be provided by a
 * caller upon its creation.  You want to preallocate memory block and use it to
 * distribute memory blocks?  Create an allocator with this behavior and provide
 * it to the collection.
 *
 * ### Predefined memory allocators
 *
 * The module defines a few allocators for you:
 *
 * - Default allocator. Use `NULL`-pointer in #coreAllocateMemory,
 *   #coreReallocateMemory, and #coreFreeMemory functions.
 * - #coreMonotonicBufferMemoryAllocator releases the allocated memory only when
 *   the resource is destroyed.  It is intended for very fast memory allocations
 *   in situations where memory is used to build up a few objects and then is
 *   released all at once.
 *
 * @{
 */

/* Statuses then belong to memory management.
 * -------------------------------------------------------------------------- */
#define CORE_FAILURE_OUT_OF_MEMORY                                             \
    coreStatusCreateFailure(coreSubsystemMemory, 0x0)

/* Types of memory allocation functions.
 * -------------------------------------------------------------------------- */

/**
 * @brief A pointer to a function that allocates a block of memory for an array
 *        of @a num elements, each of them @a size bytes long, and initializes
 *        all its bits to zero.
 *
 * @param[in] userData  A value to be interpreted by the implementation of the
 *                      callback.
 * @param[in] num       Number of elements to allocate.
 * @param[in] size      Size of each element.
 *
 * The effective result is the allocation of a zero-initialized memory block of
 * (@c num @c * @c size) bytes.
 *
 * A @ref coreMemoryAllocator "memory allocator" will pass this value
 * as the first parameter to the callback.
 *
 * @return On success, a pointer to the memory block allocated by the function.
 *         If the function failed to allocate the requested block of memory, a
 *         null pointer is returned.
 *         If @a num or @a size is zero, a null pointer is returned.
 *
 * @see #coreFreeMemoryCallback, #coreReallocateMemoryCallback,
 *      #coreAllocateMemory, and #coreMemoryAllocator.
 */
typedef void* (*coreAllocateMemoryCallback)(void* userData,
                                            usize num,
                                            usize size);

/**
 * @brief A pointer to a function that changes the size of the memory block
 *        pointed to by @a ptr.
 *
 * @param[in] userData  A value to be interpreted by the implementation of the
 *                      callback.
 * @param[in] ptr       Pointer to a memory block previously allocated with
 *                      a function of either #coreAllocateMemoryCallback
 *                      or #coreReallocateMemoryCallback type. Can be a
 *                      null pointer.
 * @param[in] size      New size for the memory block, in bytes.
 *
 * The function may move the memory block to a new location (whose address is
 * returned by the function).
 *
 * The content of the memory block is preserved up to the lesser of the new and
 * old sizes, even if the block is moved to a new location. If the new size is
 * larger, the value of the newly allocated portion is indeterminate.
 *
 * In case that @a ptr is a null pointer, the function behaves like
 * #coreAllocateMemoryCallback, assigning a new block of @a size bytes and
 * returning a pointer to its beginning.
 *
 * Otherwise, if @a size is zero, the memory previously allocated at @a ptr is
 * deallocated as if a call to #coreFreeMemoryCallback was made, and a null
 * pointer is returned.
 *
 * A @ref coreMemoryAllocator "memory allocator" will pass this value as the
 * first parameter to the callback.
 *
 * @return A pointer to the reallocated memory block, which may be either the
 * same as @a ptr or a new location. A null pointer indicates either
 * that @a size was zero (an thus @a ptr was deallocated), or that the
 * function did not allocate storage (and thus the block pointed by
 * @a ptr was not modified).
 *
 * @see #coreAllocateMemoryCallback, #coreFreeMemoryCallback,
 *      #coreReallocateMemory, and #coreMemoryAllocator.
 */
typedef void* (*coreReallocateMemoryCallback)(void* userData,
                                              void* ptr,
                                              usize size);

/**
 * @brief A pointer to a function that frees the memory space pointed
 *        to by @a ptr, which must have been returned by a previous
 *        call to a function of either #coreAllocateMemoryCallback or
 *        #coreReallocateMemoryCallback type.
 *
 * @param[in] userData  A value to be interpreted by the implementation of the
 *                      callback.
 * @param[in] ptr       Pointer to a memory block previously allocated with
 *                      #coreAllocateMemoryCallback or
 *                      #coreReallocateMemoryCallback.
 *
 * If the function has already been called before, undefined behavior occurs. If
 * @a ptr is null pointer, no operation is performed.
 *
 * If @a ptr does not point to a block of memory allocated with the above
 * functions, it causes undefined behavior.
 *
 * A @ref coreMemoryAllocator "memory allocator" will pass this value as the
 * first parameter to the callback.
 *
 * @see #coreAllocateMemoryCallback, #coreReallocateMemoryCallback,
 *      #coreFreeMemory, and #coreMemoryAllocator.
 */
typedef void (*coreFreeMemoryCallback)(void* userData, void* ptr);

/* Memory allocator API.
 * -------------------------------------------------------------------------- */

/**
 * @brief A memory allocator is a group of memory allocation functions.
 *
 * @see #coreResolveMemoryAllocator, #coreAllocateMemory,
 *      #coreReallocateMemory, and #coreFreeMemory.
 */
typedef struct coreMemoryAllocator
{
    /**
     * @brief A value to be interpreted by the implementation of the callbacks.
     *
     * The allocator will pass this value as the first parameter to the
     * callbacks.
     */
    void* userData;

    /**
     * @brief A function that allocates a block of memory and initializes all
     *        its bits to zero.
     *
     * It cannot be a null pointer.
     */
    coreAllocateMemoryCallback allocate;

    /**
     * @brief A function that changes the size of the memory block, previously
     *        allocated by #allocate.
     *
     * It cannot be a null pointer.
     */
    coreReallocateMemoryCallback reallocate;

    /**
     * @brief A function that frees the memory block, previously allocated by
     *        #allocate or #reallocate.
     *
     * It cannot be a null pointer.
     */
    coreFreeMemoryCallback free;
} coreMemoryAllocator;

/**
 * @brief Fallbacks to the default memory allocator if the given one is null
 *        pointer.
 *
 * @param[in]  allocator A memory allocator or null pointer.
 *
 * @return Default memory allocator if the given @a allocator is a null pointer
 *         or the @a allocator itself otherwise.
 *
 * @par Panics
 *
 * @li If any member function of the @a allocator is a null pointer.
 *
 * @see #coreMemoryAllocator.
 */
MOLFAR_CORE_EXPORT
coreMemoryAllocator*
coreResolveMemoryAllocator(coreMemoryAllocator* allocator);

/* Polymorphic memory allocation functions.
 * -------------------------------------------------------------------------- */

/**
 * @brief Allocates a block of memory for an array of @a num elements, each of
 *        them @a size bytes long, and initializes all its bits to zero.
 *
 * @param[in] allocator  A memory allocator that does actual job.
 * @param[in] num        Number of elements to allocate.
 * @param[in] size       Size of each element.
 *
 * The effective result is the allocation of a zero-initialized memory block of
 * (@c num @c * @c size) bytes.
 *
 * If @a allocator is a null pointer then the function fallbacks to
 * the default memory allocator.
 *
 * @return On success, a pointer to the memory block allocated by the function.
 *         If the function failed to allocate the requested block of memory,
 *         a null pointer is returned.
 *         If @a num or @a size is zero, a null pointer is returned.
 *
 * @see #coreReallocateMemory, #coreFreeMemory and #coreMemoryAllocator.
 */
MOLFAR_CORE_EXPORT
void*
coreAllocateMemory(coreMemoryAllocator* allocator, usize num, usize size);

/**
 * @brief Changes the size of the memory block pointed to by @a ptr.
 *
 * @param[in] allocator  A memory allocator that does the actual job.
 * @param[in] ptr        Pointer to a memory block previously allocated with
 *                       #coreAllocateMemory or #coreReallocateMemory. Can be
 *                       a null pointer.
 * @param[in] size       New size for the memory block, in bytes.
 *
 * The function may move the memory block to a new location (whose address is
 * returned by the function).
 *
 * The content of the memory block is preserved up to the lesser of the new and
 * old sizes, even if the block is moved to a new location. If the new size is
 * larger, the value of the newly allocated portion is indeterminate.
 *
 * In case that @a ptr is a null pointer, the function behaves like
 * #coreAllocateMemory, assigning a new block of @a size bytes and returning a
 * pointer to its beginning.
 *
 * Otherwise, if @a size is zero, the memory previously allocated at @a ptr is
 * deallocated as if a call to #coreFreeMemory was made, and a null pointer is
 * returned.
 *
 * If @a allocator is a null pointer then the function fallbacks to
 * the default memory allocator.
 *
 * @return A pointer to the reallocated memory block, which may be either the
 *         same as @a ptr or a new location. A null pointer indicates either
 *         that @a size was zero (an thus @a ptr was deallocated), or that the
 *         function did not allocate storage (and thus the block pointed
 *         by @a ptr was not modified).
 *
 * @see #coreAllocateMemory and #coreFreeMemory.
 */
MOLFAR_CORE_EXPORT
void*
coreReallocateMemory(coreMemoryAllocator* allocator, void* ptr, usize size);

/**
 * @brief Frees the memory space pointed to by @a ptr, which must have been
 *        returned by a previous call to #coreAllocateMemory or
 *        #coreReallocateMemory.
 *
 * @param[in] allocator  A memory allocator that does the actual job.
 * @param[in] ptr        A pointer to a memory block previously allocated with
 *                       #coreAllocateMemory or #coreReallocateMemory.
 *
 * If the function has already been called before, undefined behavior occurs. If
 * @a ptr is null pointer, no operation is performed.
 *
 * If @a ptr does not point to a block of memory allocated with the above
 * functions, it causes undefined behavior.
 *
 * If @a allocator is a null pointer then the function fallbacks to
 * the default memory allocator.
 *
 * @see #coreAllocateMemory and #coreReallocateMemory.
 */
MOLFAR_CORE_EXPORT
void
coreFreeMemory(coreMemoryAllocator* allocator, void* ptr);

/* Utility functions.
 * -------------------------------------------------------------------------- */

/**
 * @brief Checks whether @a p is a null pointer.
 *
 * @param[in] p  A pointer to check.
 *
 * Returns @c true if @a p is a null pointer and @c false otherwise.
 */
#define coreIsNull(p) (NULL == (p))

/**
 * @brief Checks whether @a p is @b not a null pointer.
 *
 * @param[in] p  A pointer to check.
 *
 * Returns @c true if @a p is @b not a null pointer and @c false otherwise.
 */
#define coreIsNotNull(p) !coreIsNull(p)

/*
 * TODO: Add the following features:
 *       - FillMemory(void *ptr, u8 value, usize num);
 *       - CopyMemory(void *dst, void const * src, usize num);
 *       - FindByteInMemory(void const * ptr, u8 value, usize num);
 *       - MoveMemory(void *dst, void const *src, usize num);
 *       - ComparisionResult
 *       - CompareMemory(void const *a, void const *b, usize num);
 */

/**
 * @}
 */

/**
 * @}
 */

MOLFAR_CORE_EXTERN_C_END

#endif /* MOLFAR_CORE_MEMORY_H */

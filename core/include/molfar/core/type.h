/* SPDX-License-Identifier: Apache-2.0 */

/*
 * File:             include/molfar/core/type.h
 * Created:          14-02-2019
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2019 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MOLFAR_CORE_TYPE_H
#define MOLFAR_CORE_TYPE_H 1

#include <molfar/core/lib.h>
#include <molfar/core/memory.h>
#include <molfar/core/panic.h>
#include <molfar/core/primitive_types.h>

MOLFAR_CORE_EXTERN_C_BEGIN

/**
 * @file
 *
 * @brief Runtime type information.
 */

/* coreType callbacks.
 * -------------------------------------------------------------------------- */

typedef void (*coreTypeTerminateCallback)(void* instance);
typedef void (*coreTypeDefaultInitCallback)(void* instance);
typedef void (*coreTypeCopyCallback)(void const* source, void* destination);

typedef struct coreTypeCallbacks
{
    coreTypeDefaultInitCallback defaultInit;
    coreTypeTerminateCallback   terminate;
    coreTypeCopyCallback        copy;
} coreTypeCallbacks;

/* coreType data type.
 * -------------------------------------------------------------------------- */

typedef struct coreType
{
    char const*             name;
    char const*             description;
    usize                   size;
    coreTypeCallbacks const callbacks;
} coreType;

/* coreType functions.
 * -------------------------------------------------------------------------- */

inline bool
coreTypeHasDefaultInit(coreType const* const type)
{
    corePanicIfNull(type);

    return coreIsNotNull(type->callbacks.defaultInit);
}

inline bool
coreTypeHasTerminate(coreType const* const type)
{
    corePanicIfNull(type);

    return coreIsNotNull(type->callbacks.terminate);
}

inline bool
coreTypeHasCopy(coreType const* const type)
{
    corePanicIfNull(type);

    return coreIsNotNull(type->callbacks.copy);
}

inline void
coreTypeDefaultInitInstance(coreType const* const type, void* instance)
{
    corePanicIfNull(instance);

    if (coreTypeHasDefaultInit(type)) {
        type->callbacks.defaultInit(instance);
    }
}

inline void
coreTypeTerminateInstance(coreType const* const type, void* instance)
{
    corePanicIfNull(instance);

    if (coreTypeHasTerminate(type)) {
        type->callbacks.terminate(instance);
    }
}

inline void
coreTypeCopyInstance(coreType const* const type,
                     void const*           source,
                     void*                 destination)
{
    corePanicIfNull(source);
    corePanicIfNull(destination);

    if (coreTypeHasCopy(type)) {
        type->callbacks.copy(source, destination);
    } else {
        /* TODO: Copy raw bits? */
    }
}

/* Runtime type information helper macros.
 * -------------------------------------------------------------------------- */

#define MOLFAR_CORE_RTTI_DECLARE(type) extern coreType const* const type##_RTTI

#define MOLFAR_CORE_RTTI_DEFINE(type, typeDescription, typeCallbacks)          \
    coreType _molfar_core_rtti_for_##type = { .name        = #type,            \
                                              .description = typeDescription,  \
                                              .size        = sizeof(type),     \
                                              .callbacks   = typeCallbacks };    \
    /* The comment to make clang-format work correctly */                      \
    coreType const* const type##_RTTI = &_molfar_core_rtti_for_##type

/* Runtime type information for molfar/core primitive types.
 * -------------------------------------------------------------------------- */

MOLFAR_CORE_EXPORT MOLFAR_CORE_RTTI_DECLARE(i8);
MOLFAR_CORE_EXPORT MOLFAR_CORE_RTTI_DECLARE(i16);
MOLFAR_CORE_EXPORT MOLFAR_CORE_RTTI_DECLARE(i32);
MOLFAR_CORE_EXPORT MOLFAR_CORE_RTTI_DECLARE(i64);

MOLFAR_CORE_EXPORT MOLFAR_CORE_RTTI_DECLARE(u8);
MOLFAR_CORE_EXPORT MOLFAR_CORE_RTTI_DECLARE(u16);
MOLFAR_CORE_EXPORT MOLFAR_CORE_RTTI_DECLARE(u32);
MOLFAR_CORE_EXPORT MOLFAR_CORE_RTTI_DECLARE(u64);

MOLFAR_CORE_EXPORT MOLFAR_CORE_RTTI_DECLARE(usize);

MOLFAR_CORE_EXPORT MOLFAR_CORE_RTTI_DECLARE(f32);
MOLFAR_CORE_EXPORT MOLFAR_CORE_RTTI_DECLARE(f64);

MOLFAR_CORE_EXTERN_C_END

#endif /* MOLFAR_CORE_TYPE_H */

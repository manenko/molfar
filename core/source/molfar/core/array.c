/* SPDX-License-Identifier: Apache-2.0 */

/*
 * File:             source/molfar/core/array.c
 * Created:          13-02-2019
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2019 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <molfar/core/array.h>

struct coreArray
{
    void*                buffer;
    usize                length;
    usize                capacity;
    coreMemoryAllocator* allocator;
    coreType const*      elementType;
};

coreArray*
coreArrayCreate(coreType const* elementType, coreMemoryAllocator* allocator)
{
    usize defaultCapacity = 0;

    return coreArrayCreateWithCapacity(elementType, defaultCapacity, allocator);
}

coreArray*
coreArrayCreateWithCapacity(coreType const*      elementType,
                            usize                capacity,
                            coreMemoryAllocator* allocator)
{
    corePanicIfNull(elementType);
    /* TODO: panicIfElementTypeSizeIsZero(elementType); */

    coreMemoryAllocator* alloc = coreResolveMemoryAllocator(allocator);

    void* buffer = coreAllocateMemory(alloc, capacity, elementType->size);

    if (coreIsNull(buffer)) {
        return NULL;
    }

    coreArray* array = coreAllocateMemory(alloc, sizeof(coreArray), 1);

    if (coreIsNotNull(array)) {
        coreFreeMemory(alloc, buffer);
    }

    array->allocator   = alloc;
    array->elementType = elementType;
    array->buffer      = buffer;
    array->capacity    = capacity;
    array->length      = 0;

    return array;
}

void
_coreArrayTerminateElements(coreArray* array)
{
    if (coreTypeHasTerminate(array->elementType)) {
        coreTypeTerminateCallback terminate =
          array->elementType->callbacks.terminate;

        for (usize i = 0; i < array->length; ++i) {
            terminate(coreArrayGetAt(array, i));
        }
    }
}

void
coreArrayDestroy(coreArray* array)
{
    corePanicIfNull(array);

    if (coreIsNotNull(array->buffer)) {
        _coreArrayTerminateElements(array);

        coreFreeMemory(array->allocator, array->buffer);
        coreFreeMemory(array->allocator, array);
    }
}

coreStatus
coreArrayAppend(coreArray* array, void* element)
{
    corePanicIfNull(array);
    corePanicIfNull(element);

    if (array->length == array->capacity) {
        usize newCapacity = array->capacity == 0 ? 1 : array->capacity * 2;

        void* newBuffer =
          coreReallocateMemory(array->allocator, array->buffer, newCapacity);

        if (coreIsNull(newBuffer)) {
            return CORE_FAILURE_OUT_OF_MEMORY;
        }

        array->capacity = newCapacity;
        array->buffer   = newBuffer;
    }

    array->length      = array->length + 1;
    void* arrayElement = coreArrayGetAt(array, array->length - 1);

    if (coreTypeHasCopy(array->elementType)) {
        array->elementType->callbacks.copy(element, arrayElement);
    } else {
        /* The type doesn't have special copy, let's copy raw bytes */
        corePanicNotImplemented();
    }

    return CORE_SUCCESS;
}

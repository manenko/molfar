/* SPDX-License-Identifier: Apache-2.0 */

/*
 * File:             source/molfar/core/memory.c
 * Created:          10-02-2019
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2019 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <molfar/core.h>

#include <stdlib.h>

void
_sys_free(void* userData, void* ptr)
{
    free(ptr);
}

void*
_sys_alloc(void* userData, usize num, usize size)
{
    if (0 == num || 0 == size) {
        return NULL;
    }

    return calloc(num, size);
}

void*
_sys_realloc(void* userData, void* ptr, usize size)
{
    if (0 == size) {
        _sys_free(userData, ptr);

        return NULL;
    }

    return realloc(ptr, size);
}

void
_validateAllocator(coreMemoryAllocator* allocator)
{
    corePanicIfNull(allocator);
    corePanicIfNull(allocator->allocate);
    corePanicIfNull(allocator->free);
    corePanicIfNull(allocator->reallocate);
}

coreMemoryAllocator*
coreResolveMemoryAllocator(coreMemoryAllocator* allocator)
{
    static coreMemoryAllocator defaultAllocator = { .allocate   = _sys_alloc,
                                                    .reallocate = _sys_realloc,
                                                    .free       = _sys_free,
                                                    .userData   = NULL };

    if (coreIsNull(allocator)) {
        return &defaultAllocator;
    } else {
        _validateAllocator(allocator);

        return allocator;
    }
}

void*
coreAllocateMemory(coreMemoryAllocator* a, usize num, usize size)
{
    coreMemoryAllocator* allocator = coreResolveMemoryAllocator(a);

    return allocator->allocate(allocator->userData, num, size);
}

void*
coreReallocateMemory(coreMemoryAllocator* a, void* ptr, usize size)
{
    coreMemoryAllocator* allocator = coreResolveMemoryAllocator(a);

    return allocator->reallocate(allocator->userData, ptr, size);
}

void
coreFreeMemory(coreMemoryAllocator* a, void* ptr)
{
    coreMemoryAllocator* allocator = coreResolveMemoryAllocator(a);

    allocator->free(allocator->userData, ptr);
}

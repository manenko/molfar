/* SPDX-License-Identifier: Apache-2.0 */

/*
 * File:             source/molfar/core/type.c
 * Created:          14-02-2019
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2019 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <molfar/core/type.h>

#define MOLFAR_CORE_DEFINE_NUMBER_TYPE_CALLBACKS_FUNCTIONS(type)               \
    void type##DefaultInit(void* instance) { *((type*)instance) = 0; }         \
                                                                               \
    void type##Copy(void const* source, void* destination)                     \
    {                                                                          \
        type const* s = source;                                                \
        type*       d = destination;                                           \
        *d            = *s;                                                    \
    }

#define MOLFAR_CORE_NUMBER_TYPE_CALLBACKS(type)                                \
    {                                                                          \
        .defaultInit = type##DefaultInit, .copy = type##Copy,                  \
        .terminate = NULL                                                      \
    }

#define MOLFAR_CORE_RTTI_DEFINE_NUMERIC(type, description)                     \
    MOLFAR_CORE_DEFINE_NUMBER_TYPE_CALLBACKS_FUNCTIONS(type)                   \
    MOLFAR_CORE_RTTI_DEFINE(                                                   \
      type, description, MOLFAR_CORE_NUMBER_TYPE_CALLBACKS(type));

MOLFAR_CORE_RTTI_DEFINE_NUMERIC(i8, "The 8-bit signed integer type.");
MOLFAR_CORE_RTTI_DEFINE_NUMERIC(i16, "The 16-bit signed integer type.");
MOLFAR_CORE_RTTI_DEFINE_NUMERIC(i32, "The 32-bit signed integer type.");
MOLFAR_CORE_RTTI_DEFINE_NUMERIC(i64, "The 64-bit signed integer type.");

MOLFAR_CORE_RTTI_DEFINE_NUMERIC(u8, "The 8-bit unsigned integer type.");
MOLFAR_CORE_RTTI_DEFINE_NUMERIC(u16, "The 16-bit unsigned integer type.");
MOLFAR_CORE_RTTI_DEFINE_NUMERIC(u32, "The 32-bit unsigned integer type.");
MOLFAR_CORE_RTTI_DEFINE_NUMERIC(u64, "The 64-bit unsigned integer type.");

MOLFAR_CORE_RTTI_DEFINE_NUMERIC(
  usize,
  "The pointer-sized unsigned integer type. It has the same size as a pointer "
  "on a target machine");

MOLFAR_CORE_RTTI_DEFINE_NUMERIC(f32, "The 32-bit floating-point type.");
MOLFAR_CORE_RTTI_DEFINE_NUMERIC(f64, "The 64-bit floating-point type.");

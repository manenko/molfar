/* SPDX-License-Identifier: Apache-2.0 */

/*
 * File:             include/molfar/test/lib.h
 * Created:          10-02-2019
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2019 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MOLFAR_TEST_LIB_H
#define MOLFAR_TEST_LIB_H 1

/**
 * @file
 *
 * @brief Defines platform-specific export macros for molfar/test library.
 */

#ifndef DOXYGEN_SHOULD_SKIP_THIS
#ifdef __cplusplus
#define MOLFAR_TEST_EXTERN_C_BEGIN                                             \
    extern "C"                                                                 \
    {
#define MOLFAR_TEST_EXTERN_C_END }
#else
#define MOLFAR_TEST_EXTERN_C_BEGIN
#define MOLFAR_TEST_EXTERN_C_END
#endif /* __cplusplus */
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

MOLFAR_TEST_EXTERN_C_BEGIN
#include <molfar/test/export.h>
MOLFAR_TEST_EXTERN_C_END

typedef struct coreString
{
    long dummy;
} coreString;

typedef struct coreArray
{
    long dummy;
} coreArray;

struct testTestRun;

typedef void (*testTestFunction)();

typedef struct testTestCase
{
    coreString       id;
    coreString       description;
    testTestFunction function;
} testTestCase;

typedef enum testTestRunResult
{
    testTestRunResultFailed    = 0x0,
    testTestRunResultSucceeded = 0x1,
    testTestRunResultAborted   = 0x2,
} testTestRunResult;

typedef struct testTestRun
{
    testTestCase      testCase;
    testTestRunResult result;
} testTestRun;

#include <stdlib.h>

void
testTestRunExecute(testTestRun* testRun);

void
testTestRunExecuteAll(coreArray* testRuns);

#endif /* MOLFAR_TEST_LIB_H */

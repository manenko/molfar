/* SPDX-License-Identifier: Apache-2.0 */

/*
 * File:             include/molfar/test.h
 * Created:          10-02-2019
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2019 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MOLFAR_TEST_H
#define MOLFAR_TEST_H 1

/**
 * @defgroup molfar_test test
 * @brief Unit testing library.
 * @{
 */



/**
 * @}
 */


#include <molfar/test/lib.h>

#endif /* MOLFAR_TEST_H */

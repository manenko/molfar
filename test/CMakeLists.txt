cmake_minimum_required(VERSION 3.12 FATAL_ERROR)


project(molfar-test VERSION 0.1.0 LANGUAGES C)


include(GenerateExportHeader)
include(GNUInstallDirs)


add_library(${PROJECT_NAME})


generate_export_header(${PROJECT_NAME}
  EXPORT_FILE_NAME ${PROJECT_BINARY_DIR}/include/molfar/test/export.h)


set(INCLUDE_ROOT include)
set(SOURCE_ROOT  source)


set(INCLUDE_FILES
  ${INCLUDE_ROOT}/molfar/test.h
  ${INCLUDE_ROOT}/molfar/test/export.h
  ${INCLUDE_ROOT}/molfar/test/lib.h
  )


set(SOURCE_FILES
  ${SOURCE_ROOT}/molfar/test/lib.c
  )


target_sources(${PROJECT_NAME}
  PRIVATE

  ${INCLUDE_FILES}
  ${SOURCE_FILES})


source_group(
  TREE   ${CMAKE_CURRENT_SOURCE_DIR}/${INCLUDE_ROOT}
  PREFIX ${INCLUDE_ROOT}
  FILES  ${INCLUDE_FILES})


source_group(
  TREE   ${CMAKE_CURRENT_SOURCE_DIR}/${SOURCE_ROOT}
  PREFIX ${SOURCE_ROOT}
  FILES  ${SOURCE_FILES})


target_include_directories(${PROJECT_NAME}
  PUBLIC
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/include>
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>

  PRIVATE
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/source>
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/source>)


target_compile_features(${PROJECT_NAME}
  PUBLIC
  c_std_11
  c_static_assert
  c_variadic_macros)

target_compile_options(${PROJECT_NAME}
  PRIVATE

  # Clang/GCC warnings
  $<$<OR:$<CXX_COMPILER_ID:Clang>,$<CXX_COMPILER_ID:GNU>>:-Wall>

  # MSVC warnings
  $<$<CXX_COMPILER_ID:MSVC>:/W4>)

target_link_libraries(${PROJECT_NAME}
  PRIVATE
  ${CMAKE_DL_LIBS})


install(
  TARGETS  ${PROJECT_NAME}
  EXPORT   ${PROJECT_NAME}Targets
  LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR})


install(
  DIRECTORY   include/
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})


install(
  EXPORT      ${PROJECT_NAME}Targets
  FILE        ${PROJECT_NAME}Targets.cmake
  NAMESPACE   ${PROJECT_NAME}::
  DESTINATION lib/cmake/${PROJECT_NAME})


include(CMakePackageConfigHelpers)
write_basic_package_version_file(
  "${PROJECT_NAME}ConfigVersion.cmake"

  VERSION       ${PROJECT_VERSION}
  COMPATIBILITY SameMajorVersion)

